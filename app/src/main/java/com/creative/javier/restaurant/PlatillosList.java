package com.creative.javier.restaurant;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class PlatillosList extends Activity implements View.OnClickListener {

    private RecyclerView rvPlatillos;
    private String categoria;
    private List<InformacionCard> dataSet;
    private RecyclerViewAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Intent siguienteActivity;
    private String[] menuDesayunos;
    private String[] menuBebidas;
    private String[] menuComidas;
    private String[] menuPostres;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.platillos_menu);
        init();
    }

    private void init(){
        preferences = getApplication().getSharedPreferences("Categoria",MODE_PRIVATE);
        menuDesayunos = getResources().getStringArray(R.array.desayunos);
        menuComidas = getResources().getStringArray(R.array.comidas);
        menuPostres = getResources().getStringArray(R.array.postres);
        menuBebidas = getResources().getStringArray(R.array.bebidas);
        categoria = preferences.getString("Categoria","");
        getActionBar().setTitle(categoria);
        rvPlatillos = findViewById(R.id.recyclerViewAlimentos);
        rvPlatillos.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvPlatillos.setLayoutManager(layoutManager);
        dataSet = llenarLista(categoria);
        adapter = new RecyclerViewAdapter(dataSet, this, new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(InformacionCard item) {
                cambiarActivity(item);
            }
        });
        rvPlatillos.setAdapter(adapter);
    }

    private void cambiarActivity(InformacionCard item){
        //Toast.makeText(this,"platillo"+item.getPlatillo(),Toast.LENGTH_SHORT).show();
        siguienteActivity = new Intent(this,DescripcionPlatillo.class);
        siguienteActivity.putExtra("Alimento",item.getPlatillo());
        siguienteActivity.putExtra("IdImagen",item.getIdImagen());
        startActivity(siguienteActivity);
    }

    private List<InformacionCard> llenarLista(String cat){
        List<InformacionCard> informacion = new ArrayList<InformacionCard>();
        switch (categoria){
            case "Bebidas":
                for(int i = 0; i < menuBebidas.length; i++){
                    InformacionCard info = new InformacionCard();
                    info.setCategoria("Bebidas");
                    info.setPlatillo(menuBebidas[i]);
                    info.setIdImagen(R.drawable.bebidas);
                    informacion.add(info);
                }
                break;
            case "Comidas":
                for(int i = 0; i < menuComidas.length; i++){
                    InformacionCard info = new InformacionCard();
                    info.setCategoria("Comidas");
                    info.setPlatillo(menuComidas[i]);
                    info.setIdImagen(R.drawable.comida);
                    informacion.add(info);
                }
                break;
            case "Desayunos":
                for(int i = 0; i < menuDesayunos.length; i++){
                    InformacionCard info = new InformacionCard();
                    info.setCategoria("Desayunos");
                    info.setPlatillo(menuDesayunos[i]);
                    info.setIdImagen(R.drawable.desayuno);
                    informacion.add(info);
                }
                break;
            case "Postres":
                for(int i = 0; i < menuPostres.length; i++){
                    InformacionCard info = new InformacionCard();
                    info.setCategoria("Postres");
                    info.setPlatillo(menuPostres[i]);
                    info.setIdImagen(R.drawable.postres);
                    informacion.add(info);
                }
                break;
        }
        return  informacion;
    }

    @Override
    public void onClick(View v) {

    }
}
