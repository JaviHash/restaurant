package com.creative.javier.restaurant;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    public interface OnItemClickListener {
        void onItemClick(InformacionCard item);
    }

    private List<InformacionCard> dataSet;
    private Context context;
    private OnItemClickListener listener;

    public RecyclerViewAdapter(List<InformacionCard> dataSet, Context context, OnItemClickListener listener){
        this.context = context;
        this.dataSet = dataSet;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_template,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder holder, int position) {
        holder.bind(dataSet.get(position),listener);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView ivIcono;
        private TextView tvCategoria;
        private TextView tvPlatillo;

        public ViewHolder(View itemView) {
            super(itemView);
            ivIcono = itemView.findViewById(R.id.ivImagenAlimento);
            tvCategoria = itemView.findViewById(R.id.tvCategoriaItem);
            tvPlatillo = itemView.findViewById(R.id.tvNombreAlimento);
        }

        public void bind(final InformacionCard item, final OnItemClickListener listener){
            ivIcono.setImageResource(item.getIdImagen());
            tvCategoria.setText(item.getCategoria());
            tvPlatillo.setText(item.getPlatillo());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}