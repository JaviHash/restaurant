package com.creative.javier.restaurant;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

public class Categorias extends Activity implements View.OnClickListener {
    private CardView cvDesayunos;
    private CardView cvBebidas;
    private CardView cvComidas;
    private CardView cvPostres;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Intent siguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categorias);
        init();
    }

    private void init(){
        cvBebidas = findViewById(R.id.cvBebidas);
        cvComidas = findViewById(R.id.cvComidas);
        cvPostres = findViewById(R.id.cvPostres);
        cvDesayunos = findViewById(R.id.cvDesayunos);
        preferences = getApplicationContext().getSharedPreferences("Categoria", MODE_PRIVATE);
        editor = preferences.edit();

        cvDesayunos.setOnClickListener(this);
        cvBebidas.setOnClickListener(this);
        cvComidas.setOnClickListener(this);
        cvPostres.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        siguiente = new Intent(this,PlatillosList.class);
        switch (v.getId()){
            case R.id.cvBebidas:
                //Toast.makeText(this,preferences.getString("Categoria",""),Toast.LENGTH_SHORT).show();
                editor.putString("Categoria","Bebidas");
                editor.apply();
                startActivity(siguiente);
                break;
            case R.id.cvComidas:
                //Toast.makeText(this,preferences.getString("Categoria",""),Toast.LENGTH_SHORT).show();
                editor.putString("Categoria","Comidas");
                editor.apply();
                startActivity(siguiente);
                break;
            case R.id.cvPostres:
                //Toast.makeText(this,preferences.getString("Categoria",""),Toast.LENGTH_SHORT).show();
                editor.putString("Categoria","Postres");
                editor.apply();
                startActivity(siguiente);
                break;
            case R.id.cvDesayunos:
                //Toast.makeText(this,preferences.getString("Categoria",""),Toast.LENGTH_SHORT).show();
                editor.putString("Categoria","Desayunos");
                editor.apply();
                startActivity(siguiente);
                break;


        }
    }
}
