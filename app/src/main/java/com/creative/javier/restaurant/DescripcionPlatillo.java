package com.creative.javier.restaurant;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DescripcionPlatillo extends Activity {
    private String platillo;
    private int idImagen;
    private ImageView ivPlatillo;
    private TextView tvPlatillo;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.descripcion_platillo);
        init();
    }

    private void init(){
        platillo = getIntent().getStringExtra("Alimento");
        idImagen = getIntent().getIntExtra("IdImagen",0);
        ivPlatillo = findViewById(R.id.ivPlatilloDescripcion);
        ivPlatillo.setImageResource(idImagen);
        getActionBar().setTitle(platillo);
        tvPlatillo = findViewById(R.id.tvPlatillo);
        tvPlatillo.setText(platillo);
    }
}
