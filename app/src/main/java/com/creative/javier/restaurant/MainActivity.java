package com.creative.javier.restaurant;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class MainActivity extends Activity implements View.OnClickListener {
    private CardView cvMenu;
    private Intent siguienteActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        init();
    }

    private void init(){
        cvMenu = findViewById(R.id.cvMenu);
        cvMenu.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        siguienteActivity = new Intent(this,Categorias.class);
        startActivity(siguienteActivity);
    }
}
